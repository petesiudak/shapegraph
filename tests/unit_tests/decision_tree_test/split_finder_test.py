from app.decision_tree.split_finder import SplitFinder


class SplitFinderTest:
    VALID_TRAINING_SET = [
        [3, 22, 0.5, 'Triangle'],
        [3, 31.64, 0.51, 'Triangle'],
        [4, 16, 1, 'Square'],
        [4, 17.58, 1, 'Rectangle'],
        [4, 18.11, 1, 'Rectangle'],
        [4, 16.17, 1, 'Rectangle'],
        [5, 15.9, 0.69, 'Pentagon'],
        [5, 16.2, 0.69, 'Pentagon'],
        [14, 14, 0.79, 'Circle'],
        [16, 13.97, 0.79, 'Circle'],
        [8, 26.46, 0.79, 'Ellipse'],
        [8, 32.24, 0.79, 'Ellipse'],
        [12, 22.46, 0.79, 'Ellipse'],
        [12, 14.27, 0.78, 'Ellipse'],
        [12, 18.49, 0.78, 'Ellipse'],
        [12, 16.92, 0.78, 'Ellipse'],
        [15, 14.99, 0.76, 'Ellipse']
    ]
    INVALID_TRAINING_SET = ["[3, 22, 0.5, 'Triangle']"]
    EXPECTED_RESULT = (0.23308342945021143, "Is vertices >= 8?")

    def test_split_finder_returns_expected_result(
        self,
        split_finder: SplitFinder
    ) -> None:
        best_gain, best_question = split_finder.find_best_split(
            self.VALID_TRAINING_SET
        )
        assert (best_gain, str(best_question)) == self.EXPECTED_RESULT


