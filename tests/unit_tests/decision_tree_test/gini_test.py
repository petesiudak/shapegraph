from typing import List

import pytest

from app.decision_tree.splitting_rules.gini import Gini


class GiniTest:
    @pytest.mark.parametrize(
        "labels, expected_impurity", [
            ([["Triangle"], ["Triangle"]], 0.0),
            ([["Triangle"], ["Square"]], 0.5),
            ([["Triangle"], ["Square"], ["Rectangle"]], 0.6666666666666665),
        ]
    )
    def test_gini_returns_expected_result(
        self,
        labels: List,
        expected_impurity: float
    ) -> None:
        gini_impurity = Gini().calculate_impurity(labels)
        assert gini_impurity == expected_impurity
