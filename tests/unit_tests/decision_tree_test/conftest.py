from typing import Dict

import pytest

from app.decision_tree.split_finder import SplitFinder
from app.decision_tree.splitting_rules.gini import Gini


@pytest.fixture(scope="module")
def types_and_header() -> Dict:
    return {
        "types": (int, float),
        "header": ["vertices", "thinness", "extent", "label"]
    }


@pytest.fixture(scope="module")
def split_finder(types_and_header: Dict) -> SplitFinder:
    return SplitFinder(
        Gini(),
        **types_and_header
    )
