from typing import List

import pytest

from app.decision_tree.info_gain import calculate_info_gain
from app.decision_tree.splitting_rules.gini import Gini


class InfoGainTest:
    @pytest.mark.parametrize(
        "true_rows, false_rows, uncertainty, expected_info_gain", [(
            [[5, 15.9, 0.69, "Pentagon"]], [], 0.0, 0.0
        ), (
            [[4, 16, 1, "Square"]], [[3, 22, 0.5, "Triangle"]], 0.5, 0.5
        ), (
            [[14, 14, 0.79, 'Circle'], [8, 26.46, 0.79, 'Ellipse']],
            [[3, 22, 0.5, 'Triangle'], [4, 16, 1, 'Square'], [5, 16.2, 0.69, 'Pentagon']],
            0.7999999999999998,
            0.19999999999999996
        ), (
            [[5, 15.9, 0.69, "Pentagon"]],
            [[3, 22, 0.5, "Triangle"], [4, 16, 1, "Square"]],
            0.6666666666666665,
            0.33333333333333315
        )]
    )
    def test_info_gain_returns_expected_result(
        self,
        true_rows: List,
        false_rows: List,
        uncertainty: float,
        expected_info_gain: float
    ) -> None:
        info_gain = calculate_info_gain(
            left=true_rows,
            right=false_rows,
            uncertainty=uncertainty,
            splitting_rule=Gini()
        )
        assert info_gain == expected_info_gain
