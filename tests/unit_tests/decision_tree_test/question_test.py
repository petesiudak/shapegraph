from typing import Dict, List

import pytest

from app.decision_tree.question import Question
from app.exceptions import MalformedTrainingSet


class QuestionTest:
    COLUMN = 0
    VALUE = "15"

    @pytest.mark.parametrize(
        "column_and_value, row, expected_result", [(
            {"column": 0, "value": 3},
            [3, 22, 0.5, "Triangle"],
            True
        ), (
            {"column": 1, "value": 21},
            [4, 16, 1, "Square"],
            False
        ), (
            {"column": 2, "value": 0.6},
            [14, 14, 0.79, "Circle"],
            True
        ), (
            {"column": 3, "value": "Ellipse"},
            [15, 14.99, 0.76, "Ellipse"],
            True
        )]
    )
    def test_question_returns_expected_result(
        self,
        column_and_value: Dict,
        row: List,
        expected_result: bool,
        types_and_header: Dict,
    ) -> None:
        question = Question(
            **column_and_value,
            **types_and_header
        )
        assert question.is_match(row) is expected_result

    def test_question_raises_exception_when_malformed_training_set_row(
        self,
        types_and_header: Dict,
    ) -> None:
        question = Question(
            column=self.COLUMN,
            value=self.VALUE,
            **types_and_header
        )
        with pytest.raises(MalformedTrainingSet):
            question.is_match([15, 14.99, 0.76, "Ellipse"])
