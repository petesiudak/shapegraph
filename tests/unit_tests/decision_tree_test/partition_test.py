from typing import Dict, Tuple

import pytest

from app.decision_tree.partiction import partition
from app.decision_tree.question import Question


class PartitionTest:
    TRAINING_SET = [
        [3, 22, 0.5, "Triangle"],
        [4, 16, 1, "Square"],
        [5, 15.9, 0.69, "Pentagon"]
    ]

    @pytest.mark.parametrize(
        "column_and_value, expected_true_rows, expected_false_rows", [(
            {"column": 0, "value": 5},
            [[5, 15.9, 0.69, "Pentagon"]],
            [[3, 22, 0.5, "Triangle"], [4, 16, 1, "Square"]]
        ), (
            {"column": 1, "value": 17},
            [[3, 22, 0.5, "Triangle"]],
            [[4, 16, 1, "Square"], [5, 15.9, 0.69, "Pentagon"]]
        ), (
            {"column": 3, "value": "Pentagon"},
            [[5, 15.9, 0.69, "Pentagon"]],
            [[3, 22, 0.5, "Triangle"], [4, 16, 1, "Square"]]
        )]
    )
    def test_partition_returns_expected_result(
        self,
        column_and_value: Dict,
        expected_true_rows: Tuple,
        expected_false_rows: Tuple,
        types_and_header: Dict,
    ) -> None:
        question = Question(
            **column_and_value,
            **types_and_header
        )
        true_rows, false_rows \
            = partition(rows=self.TRAINING_SET, question=question)

        assert (true_rows, false_rows) \
               == (expected_true_rows, expected_false_rows)
