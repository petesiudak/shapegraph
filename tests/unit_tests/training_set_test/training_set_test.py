from typing import List

import pytest

from app.training_set import convert_types, convert_training_set, \
    clean_training_set


def test_clean_training_set_returns_expected_result() -> None:
    training_set = clean_training_set("3 22 0.5 Triangle\r\n4 16 1 Square\r\n")
    assert training_set == ['3 22 0.5 Triangle', '4 16 1 Square']


@pytest.mark.parametrize(
    "features, expected_result", [
        (['3', '22', '0.5', 'Triangle'], [3, 22, 0.5, 'Triangle']),
        (['8', '26', '0.79', 'Ellipse'], [8, 26, 0.79, 'Ellipse'])
    ]
)
def test_convert_types_returns_expected_result(
    features: List,
    expected_result: List
) -> None:
    training_set = convert_types(features)
    assert training_set == expected_result


def test_convert_training_set_returns_expected_result() -> None:
    training_set = convert_training_set([
        "3 22 0.5 Triangle",
        "3 31.64 0.51 Triangle",
        "4 16 1 Square",
    ])
    assert training_set == [
        [3, 22, 0.5, 'Triangle'],
        [3, 31.64, 0.51, 'Triangle'],
        [4, 16, 1, 'Square'],
    ]
