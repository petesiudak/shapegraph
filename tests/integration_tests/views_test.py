import pytest
from flask.testing import FlaskClient
from werkzeug.datastructures import FileStorage


@pytest.mark.parametrize(
    "value", [
        "Geometric figures detection",
        "with decision tree learning",
        "Run",
        "Training set",
        "Decision tree will be displayed here..."
    ]
)
def test_index_returns_expected_content(
    client: FlaskClient,
    value: str
) -> None:
    response = client.get("/")

    assert response.status_code == 200
    assert value.encode() in response.data


@pytest.mark.parametrize(
    "value", [
        "Geometric figures detection",
        "with decision tree learning",
        "Run",
        "Training set",
        "Decision Tree"
    ]
)
def test_post_image_with_training_set_returns_expected_content(
    client: FlaskClient,
    value: str,
    image: FileStorage,
    training_set: FileStorage
) -> None:
    response = client.post(
        "/",
        content_type="multipart/form-data",
        data={"training-set": training_set, "image": image}
    )

    assert response.status_code == 200
    assert value.encode() in response.data
