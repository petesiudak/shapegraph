from pathlib import Path

import pytest
from flask import Flask
from flask.testing import FlaskClient
from werkzeug.datastructures import FileStorage

from app import create_app

FILES_PATH = Path().cwd() / "integration_tests/files/"
IMAGE_NAME = "test-image.png"
TRAINING_SET_NAME = "training-set.txt"
CONTENT_TYPE = "application/octet-stream"


@pytest.fixture
def app() -> Flask:
    app = create_app()
    app.config.update({
        "TESTING": True,
        "UPLOAD_PATH": "integration_tests/files",
        "FILE_NAME": "test-image.png"
    })

    yield app


@pytest.fixture
def client(app) -> FlaskClient:
    return app.test_client()


@pytest.fixture
def image() -> FileStorage:
    path = FILES_PATH / IMAGE_NAME
    return FileStorage(
        stream=Path(path).open(mode="rb"),
        filename=IMAGE_NAME,
        content_type=CONTENT_TYPE
    )


@pytest.fixture
def training_set() -> FileStorage:
    path = FILES_PATH / TRAINING_SET_NAME
    return FileStorage(
        stream=Path(path).open(mode="rb"),
        filename=TRAINING_SET_NAME,
        content_type=CONTENT_TYPE
    )
