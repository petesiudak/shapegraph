IMAGE_NAME = geometric-shapes-detection
APP_NAME = shapegraph

.DEFAULT: help
help:
	@echo "make run"
	@echo "      Runs a development server in a docker container. To build an image add 'build=true' argument."
	@echo "make clean"
	@echo "      Removes a docker image."

run:
ifeq ($(build), true)
	@docker build --no-cache -t $(IMAGE_NAME) .
endif
	@docker run -p 8080:8080 --rm --name $(APP_NAME) $(IMAGE_NAME)

clean:
	@docker image rm $(IMAGE_NAME)
