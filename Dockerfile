FROM python:3.9-slim
MAINTAINER Piotr Siudak "siudakp@gmail.com"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0

WORKDIR /usr/src/shapegraph

COPY app /usr/src/shapegraph/app/
COPY wsgi.py .
COPY requirements.txt .

RUN apt-get update && apt-get install -y python3-opencv
RUN pip install --upgrade pip --no-cache-dir -q -r requirements.txt

EXPOSE 8080
CMD ["python", "wsgi.py"]
