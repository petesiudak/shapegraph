## Table of contents
* [Overview](#overview)
* [Technologies](#technologies)
* [Setup](#setup)
* [Usage Examples](#examples)
* [To Do](#to-do)
---

## Overview
This is an extended version of one of my academic projects. I built a program 
designed to learn geometric figures of various shapes, i.e., triangle, circle, 
ellipse, square and rectangle based on a training set. I implemented the decision 
tree learning and the candidate elimination algorithms.

The process of classification begins with the root node of the decision tree
and expands by applying a splitting rule at each non-leaf node and dividing the 
dataset into homogenous subsets. While building a decision tree, each node 
identifies a splitting condition that minimizes the class labels mixing and gives 
relatively pure subsets. In order to evaluate how good the splitting rule is,
various splitting rules have been proposed. I implemented two of them, i.e., 
Entropy and Gini Index. The latter is a default splitting rule.

---
## Technologies
* Python 3.9
* OpenCV
* Flask
* Docker

---
## Setup
A simple web interface is accessible on port 8080. Type one of the following 
commands:

* `make run build=true`
* `make clean`
  
To get more details about all the commands, type:

* `make help`

---
## Usage examples
Visit "examples" directory to have a better understanding. Once you've launched 
the web app, you can use the image containing geometric figures that is rendered 
by default. You can also upload one of the images located in "examples" directory 
or draw your own geometric figures on a black background and upload it.

A training set consists of four space separated features:

1. Vertices - the number of vertices of a particular geometric figure.
2. Thinness - the square of a geometric figure's perimeter divided by a geometric 
   figure's contour area.
3. Extent - the geometric figure's contour area divided by a detected shape's 
   minimal bounding rectangle.
4. Label - the name of the detected geometric figure.

Thinness rate distinguishes a circle from an ellipse. The rate is the smallest, 
amongst all 2D geometric figures, for circles. It distinguishes a square from 
a rectangle as well. The rate is close to 16 in case of a square and is smaller
than a rectangle's one.

Extent rate distinguishes all the remaining geometric figures as follows:

* Triangles - the rate is close to 0.5
* Rectangles - the rate fluctuates between 0.75 and 0.85
* Pentagon - the rate is very close to 0.7

You can play with the initial training set or upload one of the exemplary training 
sets located in "examples" directory to check how it affects the performance
of the geometric figures detection.
---
## To Do
* Add a decision tree JS visualization
* Add a training set generator