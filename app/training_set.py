import re
from typing import List

from app.exceptions import MalformedTrainingSet

INITIAL_TRAINING_SET = [
    "3 22 0.5 Triangle",
    "3 31.64 0.51 Triangle",
    "4 16 1 Square",
    "4 17.58 1 Rectangle",
    "4 18.11 1 Rectangle",
    "4 16.17 1 Rectangle",
    "5 15.9 0.69 Pentagon",
    "5 16.2 0.69 Pentagon",
    "14 14 0.79 Circle",
    "16 13.97 0.79 Circle",
    "8 26.46 0.79 Ellipse",
    "8 32.24 0.79 Ellipse",
    "12 22.46 0.79 Ellipse",
    "12 14.27 0.78 Ellipse",
    "12 18.49 0.78 Ellipse",
    "12 16.92 0.78 Ellipse",
    "15 14.99 0.76 Ellipse"
]


def clean_training_set(
    string: str,
    replacement: str = "\r",
    splitter: str = "\n"
) -> List[str]:
    return (
        string
        .strip()
        .replace(replacement, "")
        .split(splitter)
    )


def convert_training_set(
    training_set: List[str],
    splitter: str = " "
) -> List[List]:
    raw = [element.split(splitter) for element in training_set]
    result = []
    for features in raw:
        try:
            result.append(convert_types(features))
        except ValueError:
            raise MalformedTrainingSet
    return result


def convert_types(features: List[str]) -> List:
    int_regex = re.compile("^[+-]?[0-9]+$")
    float_regex = re.compile("[+-]?[0-9]+.[0-9]+")
    row = []

    for feature in features:
        if int_regex.match(feature):
            row.append(int(feature))
        elif float_regex.match(feature):
            row.append(float(feature))
        else:
            row.append(feature)

    return row
