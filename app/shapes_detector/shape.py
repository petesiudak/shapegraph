import cv2
from numpy.core.records import ndarray

from app.exceptions import MalformedShape


class Shape:
    def __init__(self, contour: ndarray, min_area_rect: float) -> None:
        self._contour = contour
        self._min_area_rect = min_area_rect
        area = cv2.contourArea(contour)
        if area <= 0:
            raise MalformedShape
        self._area = area

    def thinness(self) -> float:
        """This is the ratio of the square of geometric
        figure's perimeter to the geometric figure's contour area.
        """
        perimeter = cv2.arcLength(self._contour, True)
        return perimeter ** 2 / self._area

    def extent(self) -> float:
        """This is the ratio of the geometric figure's contour area
        to the detected shape's minimal bounding rectangle.
        """
        ((x, y), (w, h), r) = cv2.minAreaRect(self._contour)
        rect_area = w * h

        return self._area / rect_area
