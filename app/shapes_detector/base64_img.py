import base64
import io
from pathlib import Path

from PIL import Image
from numpy import ndarray


def ndarray_to_base64(image_array: ndarray, image_format: str) -> str:
    file_bytes = io.BytesIO()
    image = Image.fromarray(image_array)
    image.save(file_bytes, image_format)
    image_bytes = file_bytes.getvalue()

    return base64.b64encode(image_bytes).decode()


def image_to_base64(path: Path) -> str:
    with path.open(mode="rb") as img_file:
        return base64.b64encode(img_file.read()).decode()
