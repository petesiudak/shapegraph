from dataclasses import dataclass
from pathlib import Path
from typing import Tuple, Dict, Any, Union

import cv2
import numpy
from numpy.core.records import ndarray
from werkzeug.datastructures import FileStorage

from app.decision_tree.decision_node import DecisionNode
from app.exceptions import MalformedShape
from app.shapes_detector.shape import Shape
from app.tree_classifier.tree_classifier import TreeClassifier


@dataclass
class ShapesDetector:
    decision_node: DecisionNode
    min_area_rect: float
    approximation_accuracy: float
    contours_color: Tuple
    prediction_styles: Dict

    def detect_shapes(self, img: Union[str, Path, FileStorage]) -> ndarray:
        numpy_image = numpy.fromfile(file=img, dtype=numpy.uint8)
        decoded_image = cv2.imdecode(buf=numpy_image, flags=cv2.IMREAD_UNCHANGED)
        grey_image = cv2.cvtColor(src=decoded_image, code=cv2.COLOR_BGR2GRAY)
        _, thresh = cv2.threshold(src=grey_image, thresh=127, maxval=255, type=0)

        return self._detect_shapes(thresh, decoded_image)

    def _detect_shapes(self, thresh: Any, image: ndarray) -> ndarray:
        contours, _ = cv2.findContours(
            image=thresh,
            mode=cv2.RETR_TREE,
            method=cv2.CHAIN_APPROX_SIMPLE
        )

        for contour in contours:
            approx = self._approximate_curves(contour)
            try:
                prediction = self._classify_features(approx, contour)
            except MalformedShape:
                continue

            self._draw_contours(image, contour)
            self._put_text(
                image=image,
                contour=contour,
                prediction=prediction
            )

        return image

    def _approximate_curves(self, contour: ndarray) -> ndarray:
        perimeter = cv2.arcLength(curve=contour, closed=True)
        approx_accuracy = self.approximation_accuracy * perimeter

        return cv2.approxPolyDP(
            curve=contour,
            epsilon=approx_accuracy,
            closed=True
        )

    def _classify_features(self, approx: ndarray, contour: ndarray) -> str:
        vertices = len(approx)
        shape = Shape(
            contour=contour,
            min_area_rect=self.min_area_rect
        )
        thinness = shape.thinness()
        extent = shape.extent()

        return (
            TreeClassifier()
            .classify_features(
                row=[vertices, thinness, extent, "Label"],
                node=self.decision_node
            )
        )

    def _draw_contours(self, image: ndarray, contour: ndarray) -> None:
        cv2.drawContours(
            image=image,
            contours=[contour],
            contourIdx=-1,
            color=self.contours_color,
            thickness=2
        )

    def _put_text(
        self,
        image: ndarray,
        contour: ndarray,
        prediction: str
    ) -> None:
        # Get a center point for a contour
        moments = cv2.moments(array=contour)
        cX = int(moments["m10"] / moments["m00"])
        cY = int(moments["m01"] / moments["m00"])

        cv2.putText(
            img=image,
            text=prediction,
            org=(cX - 40, cY + 5),
            fontFace=cv2.FONT_HERSHEY_SIMPLEX,
            **self.prediction_styles
        )
