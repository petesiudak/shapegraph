from abc import ABC, abstractmethod


class SplittingRule(ABC):
    """A splitting rule is used to find
    the best feature to split a training set"""
    @abstractmethod
    def calculate_impurity(self, rows):
        pass
