from typing import List

from app.decision_tree.labels_count import count_labels
from app.decision_tree.splitting_rules.splitting_rule import SplittingRule


class Gini(SplittingRule):
    """Implementation of one of the proposed splitting rules."""
    def calculate_impurity(self, rows: List) -> float:
        """Calculates the Gini impurity for a given list of rows.

        :param rows: A list of rows with features from a training set.
        :return: A calculated impurity rate.
        """
        labels = count_labels(rows)
        number_of_rows = float(len(rows))
        impurity = 1

        for label in labels:
            label_probability = labels[label] / number_of_rows
            impurity -= label_probability**2

        return impurity
