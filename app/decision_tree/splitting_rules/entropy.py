from typing import List

import numpy as np

from app.decision_tree.labels_count import count_labels
from app.decision_tree.splitting_rules.splitting_rule import SplittingRule


class Entropy(SplittingRule):
    """Implementation of one of the proposed splitting rules."""
    def calculate_impurity(self, rows: List) -> float:
        """Calculates the Entropy impurity for a given list of rows.

        :param rows: A list of rows with features from a training set.
        :return: An impurity rate.
        """
        labels = count_labels(rows)
        number_of_rows = len(rows)
        entropy = 0

        for label in labels:
            label_probability = labels[label] / number_of_rows
            entropy = -(label_probability * np.log2(label_probability))

        return entropy
