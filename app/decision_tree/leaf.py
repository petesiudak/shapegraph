from typing import List

from app.decision_tree.labels_count import count_labels


class Leaf:
    """A leaf stores a dictionary containing particular label's occurrences
    in a training set that reach this leaf (e.g. {"Rectangle: 2}).
    """
    def __init__(self, rows: List) -> None:
        self.predictions = count_labels(rows)
