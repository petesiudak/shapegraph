from dataclasses import dataclass
from typing import Tuple, List

from app.decision_tree.info_gain import calculate_info_gain
from app.decision_tree.partiction import partition
from app.decision_tree.question import Question
from app.decision_tree.splitting_rules.splitting_rule import SplittingRule
from app.exceptions import MalformedTrainingSet


@dataclass
class SplitFinder:
    splitting_rule: SplittingRule
    types: Tuple
    header: List

    def find_best_split(self, rows: List) -> Tuple:
        """This method divides a decision tree. A splitting rule is used to find
        the best feature to split a training set. A preferred question is the one
        that splits the training set into the most homogeneous subset.

        :param rows: A list containing training set elements.
        :return: A tuple containing the best info gain and the best question.
        """
        uncertainty = self.splitting_rule.calculate_impurity(rows)
        number_of_columns = len(rows[0]) - 1
        best_gain = 0
        best_question = None

        for column in range(number_of_columns):
            try:
                values = set([row[column] for row in rows])
            except IndexError:
                raise MalformedTrainingSet

            for value in values:
                question = Question(column, value, self.types, self.header)
                true_rows, false_rows = partition(rows, question)
                if len(true_rows) == 0 or len(false_rows) == 0:
                    continue

                gain = calculate_info_gain(
                    true_rows,
                    false_rows,
                    uncertainty,
                    self.splitting_rule
                )
                if gain >= best_gain:
                    best_gain, best_question = gain, question

        return best_gain, best_question
