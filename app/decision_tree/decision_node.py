from dataclasses import dataclass
from typing import Union, Any

from app.decision_tree.leaf import Leaf
from app.decision_tree.question import Question


@dataclass
class DecisionNode:
    """A decision node asks a question based on which
    a training set is split into next subsets.

    It stores a reference to the question and two child nodes.
    """
    question: Question
    true_branch: Union[Leaf, Any]
    false_branch: Union[Leaf, Any]
