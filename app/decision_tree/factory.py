from app.decision_tree.decision_tree import DecisionTree
from app.decision_tree.split_finder import SplitFinder
from app.decision_tree.splitting_rules.entropy import Entropy
from app.decision_tree.splitting_rules.gini import Gini


def get_decision_tree(splitting_rule: str) -> DecisionTree:
    if splitting_rule.lower() == "gini":
        rule = Gini()
    elif splitting_rule.lower() == "entropy":
        rule = Entropy()
    else:
        raise Exception(
            f"Invalid splitting rule name: '{splitting_rule}'"
        )

    return DecisionTree(
        SplitFinder(
            rule,
            (int, float),
            ["vertices", "thinness", "extent", "label"]
        )
    )
