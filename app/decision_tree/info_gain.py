from typing import List

from app.decision_tree.splitting_rules.splitting_rule import SplittingRule


def calculate_info_gain(
    left: List,
    right: List,
    uncertainty: float,
    splitting_rule: SplittingRule
) -> float:
    """Information gain.

    Calculates the uncertainty of the starting node, minus the weighted
    impurity of two child nodes. It indicates how much information we gain
    by partitioning a training set on a particular feature.

    :param left: A list of true rows after a training set split.
    :param right: A list of false rows after a training set split.
    :param uncertainty: A calculated training set impurity.
    :param splitting_rule: One of the splitting rules like Gini Index or Entropy.
    :return: Information gain.
    """
    probability = float(len(left)) / (len(left) + len(right))

    return (
        uncertainty
        - probability
        * splitting_rule.calculate_impurity(left)
        - (1 - probability)
        * splitting_rule.calculate_impurity(right)
    )
