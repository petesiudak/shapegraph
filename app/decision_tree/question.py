from dataclasses import dataclass
from typing import Any, Tuple, List

from app.exceptions import MalformedTrainingSet


@dataclass
class Question:
    """Question partitions a training set.

    It stores a feature value which is compared to values from
    a training set during a training set partitioning.

    Attributes:
        column  A column number that is used in the training set partitioning.
        value   A value that is compared with a value from the training set.
        types   Acceptable data types in case of numeric values.
        header  A training set header e.g.
                ["Vertices", "Thinness", Extent", "Label"].

    Example:
        header = ["Vertices", "Thinness", Extent", "Label"].
        training set = [3 22 0.5 Triangle]
        question = Question(2, 0.4) => results in a question condition
        against the training set "Is extent >= 0.4"
    """
    column: int
    value: Any
    types: Tuple
    header: List

    def is_match(self, row: List) -> bool:
        value = row[self.column]
        if self._is_numeric(value):
            try:
                return value >= self.value
            except TypeError:
                raise MalformedTrainingSet(
                    f"Invalid value '{self.value}' in a training set."
                )
        return value == self.value

    def _is_numeric(self, value: Any) -> bool:
        return isinstance(value, self.types)

    def __repr__(self) -> str:
        condition = "=="
        if self._is_numeric(self.value):
            condition = ">="
        return f"Is {self.header[self.column]} {condition} {self.value}?"
