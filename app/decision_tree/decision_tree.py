from dataclasses import dataclass
from typing import List, Union

from app.decision_tree.decision_node import DecisionNode
from app.decision_tree.leaf import Leaf
from app.decision_tree.partiction import partition
from app.decision_tree.split_finder import SplitFinder


@dataclass
class DecisionTree:
    split_finder: SplitFinder
    gain_threshold: float = 0.0

    def build_tree(self, rows: List) -> Union[Leaf, DecisionNode]:
        gain, question = self.split_finder.find_best_split(rows)
        if gain == self.gain_threshold:
            return Leaf(rows)

        true_rows, false_rows = partition(rows, question)
        true_branch = self.build_tree(true_rows)
        false_branch = self.build_tree(false_rows)

        return DecisionNode(
            question,
            true_branch,
            false_branch
        )

    def to_list(
        self,
        decision_tree: List,
        node: Union[Leaf, DecisionNode],
        spacing: str = "",
    ) -> Union[List[Union[Leaf, DecisionNode]], None]:
        if isinstance(node, Leaf):
            prediction = (
                f"{spacing}🍀 "
                f"Prediction: {next(iter(node.predictions.keys()))}"
            )
            samples = (
                f"{spacing}👀 Samples: "
                f"{next(iter(node.predictions.values()))}"
            )
            decision_tree.append(f"""{prediction}\n{samples}""")
            return

        decision_tree.append(f"""{spacing}Ⓠ {node.question}""")

        decision_tree.append(f"""{spacing}---> 👍 True""")
        self.to_list(decision_tree, node.true_branch, spacing + "    ")

        decision_tree.append(f"""{spacing}---> 👎 False""")
        self.to_list(decision_tree, node.false_branch, spacing + "    ")

        return decision_tree
