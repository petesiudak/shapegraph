from collections import defaultdict
from typing import Dict, List


def count_labels(rows: List) -> Dict:
    """Counts particular labels occurrences in a training set"""
    labels = defaultdict(int)
    for row in rows:
        label = row[-1]
        labels[label] += 1
    return labels
