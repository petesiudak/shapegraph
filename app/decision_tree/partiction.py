from typing import Tuple, List

from app.decision_tree.question import Question


def partition(rows: List, question: Question) -> Tuple[List, List]:
    """Partitions a training set.

    Partitions the training set by checking whether each row
    matches a question's condition. If so, it appends the row to "true_rows",
    otherwise to "false_rows".
    """
    true_rows, false_rows = [], []
    for row in rows:
        if question.is_match(row):
            true_rows.append(row)
        else:
            false_rows.append(row)

    return true_rows, false_rows
