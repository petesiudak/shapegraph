UPLOAD_IMAGE_FORMAT = "png"

SHAPES_DETECTOR = {
    "min_area_rect": 0.0,
    "approximation_accuracy": 0.01,
    "contours_color": (255, 0, 0),
    "prediction_styles": {
        "fontScale": 0.7,
        "color": (255, 0, 0),
        "thickness": 2
    }
}
