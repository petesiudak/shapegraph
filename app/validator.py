import functools
from typing import Callable, Tuple, Dict

from flask import request, flash, render_template

from app.training_set import INITIAL_TRAINING_SET


def validate_request(f: Callable) -> Callable:
    @functools.wraps(f)
    def wrap(*args: Tuple, **kwargs: Dict) -> str:
        errors = []
        if image := request.files.get("image"):
            try:
                extension = (
                    image.filename
                    .lower()
                    .strip()
                    .split(".")[1]
                )
            except IndexError:
                errors.append("Provide a valid image file.")
            else:
                (extension in ("png", "jpg", "jpeg")
                 or errors.append("Invalid file."))

        if not any([
            request.files.get("training-set"),
            request.form.get("training-set")
        ]):
            errors.append("Provide a training set.")

        if errors:
            template = args[0].template
            for error in errors:
                flash(error)

            return render_template(
                template,
                training_set=INITIAL_TRAINING_SET
            )

        return f(*args, **kwargs)
    return wrap
