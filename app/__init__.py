from flask import Flask

from app.views import Index


def create_app() -> Flask:
    app = Flask(__name__)
    app.config.update({
        "SECRET_KEY": "secret",
        "MAX_CONTENT_LENGTH": 1024 * 1024,
        "UPLOAD_PATH": "app/static/images",
        "FILE_NAME": "geometric-figures.png"
    })
    app.add_url_rule("/", view_func=Index.as_view("home"))
    app.register_error_handler(
        Exception,
        lambda error: f"Unexpected error has occurred"
    )

    return app
