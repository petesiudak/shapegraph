class MalformedTrainingSet(Exception):
    """Raise in case of malformed training set"""


class MalformedShape(Exception):
    """Raise in case of malformed shape in the image"""
