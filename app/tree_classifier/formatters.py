from typing import Dict


def percent_str_predictions(labels: Dict) -> str:
    total = sum(labels.values()) * 1.0
    probabilities = {}

    for label in labels.keys():
        percent_format = f"{int(labels[label] / total * 100)}%"
        probabilities[label] = percent_format

    return str(probabilities)
