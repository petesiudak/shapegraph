from dataclasses import dataclass
from typing import List, Union, Callable

from app.decision_tree.decision_node import DecisionNode
from app.decision_tree.leaf import Leaf


@dataclass
class TreeClassifier:
    predictions_formatter: Callable = None

    def classify_features(
        self,
        row: List,
        node: Union[Leaf, DecisionNode, List]
    ) -> str:
        if isinstance(node, Leaf):
            if self.predictions_formatter is not None:
                return self.predictions_formatter(node.predictions)
            return str(next(iter(node.predictions.keys())))

        if node.question.is_match(row):
            return self.classify_features(row, node.true_branch)

        return self.classify_features(row, node.false_branch)
