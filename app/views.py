from pathlib import Path
from typing import Union

import cv2
from flask import render_template, Response, request, flash, current_app
from flask.views import MethodView

from app import settings
from app.decision_tree import factory
from app.exceptions import MalformedTrainingSet
from app.shapes_detector.base64_img import ndarray_to_base64
from app.shapes_detector.shapes_detector import ShapesDetector
from app.training_set import convert_training_set, INITIAL_TRAINING_SET, \
    clean_training_set
from app.validator import validate_request


class Index(MethodView):
    template = "index.html"
    temp_name = "temp.png"

    def __init__(self):
        upload_path = Path().cwd() / current_app.config['UPLOAD_PATH']
        self._upload_path = upload_path / current_app.config['FILE_NAME']
        self._temp_path = upload_path / self.temp_name

    def get(self) -> str:
        return render_template(
            self.template,
            training_set=INITIAL_TRAINING_SET
        )

    @validate_request
    def post(self) -> Union[str, Response]:
        decision_tree = factory.get_decision_tree(
            splitting_rule="gini"
        )
        training_set = INITIAL_TRAINING_SET

        try:
            training_set = clean_training_set(
                request.files["training-set"]
                .stream
                .read()
                .decode() or
                request.form["training-set"]
            )
            decision_node = decision_tree.build_tree(
                convert_training_set(training_set)
            )

        except (UnicodeDecodeError, MalformedTrainingSet) as exc:
            flash(str(exc))
            return render_template(
                self.template,
                training_set=training_set
            )

        shapes_detector = ShapesDetector(
            decision_node=decision_node,
            **settings.SHAPES_DETECTOR
        )

        image_path = self._upload_path
        if image := request.files.get("image"):
            image.save(self._temp_path)
            image_path = self._temp_path

        try:
            classified_image = shapes_detector.detect_shapes(image_path)
        except cv2.error:
            image_path.unlink()
            flash("Invalid file.")
            return render_template(
                self.template,
                training_set=training_set
            )

        if self._temp_path.is_file():
            self._upload_path.unlink()
            self._temp_path.rename(self._upload_path)

        base64_image = ndarray_to_base64(
            image_array=classified_image,
            image_format=settings.UPLOAD_IMAGE_FORMAT
        )

        decision_tree_view = decision_tree.to_list(
            ["🌳 Decision Tree 🌳\n"],
            decision_node
        )

        return render_template(
            self.template,
            training_set=training_set,
            image=base64_image,
            decision_tree=decision_tree_view
        )
